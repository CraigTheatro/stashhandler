/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theatro.stashhandler;
import java.nio.file.*;
import static java.nio.file.StandardWatchEventKinds.*;
import static java.nio.file.LinkOption.*;
import java.nio.file.attribute.*;
import java.io.*;
import java.util.*;

import com.rabbitmq.client.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cmiller
 */
public class StashHandler {
    
    static final Logger LOGGER = Logger.getLogger("THEATRO");
    
    static String centralPubHost = "amqp://shiva:vanik123@%s/theatrocentral";
    static String influxPubHost = "amqp://shiva:vanik123@%s/theatrocentral";
    static String slmPubHost = "amqp://shiva:vanik123@%s/theatrocentral";

    static ConnectionFactory centralPubFactory = new ConnectionFactory();
    static ConnectionFactory influxPubFactory = new ConnectionFactory();
    static ConnectionFactory slmPubFactory = new ConnectionFactory();

    static int nextHandler=0;
    
    static Connection pubconnection;
    static Channel pubchannel;

    static Connection ipubconnection;
    static Channel ipubchannel;

    static Connection slmpubconnection;
    static Channel slmpubchannel;

    static Connection tsmpubconnection;
    static Channel tsmpubchannel;    

    static Boolean initialized = InitRabbit();

    private final WatchService watcher;
    private final Map<WatchKey,Path> keys;
    private final boolean recursive;
    private boolean trace = false;

    
    private static Properties getProperties() {
        String userdir = System.getProperty("user.dir");
        Properties props = new Properties();
        InputStream in;
        try {
            in = new FileInputStream(userdir + File.separator + "stashhandler.properties");
            props.load(in);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            return null;
        } 
        return props;
    }
    
    static Boolean InitRabbit() {
        try {
            Properties props = getProperties();
            String nbhost = props.getProperty("northbound.host");
            String slmhost = props.getProperty("slm.host");
            String devhost = props.getProperty("device.host");
            LOGGER.log(Level.INFO, String.format("Northbound host: %s", nbhost));
            LOGGER.log(Level.INFO, String.format("SLM host: %s", slmhost));
            LOGGER.log(Level.INFO, String.format("DEV host: %s", devhost));
            centralPubFactory.setUri(String.format(centralPubHost, nbhost));
            influxPubFactory.setUri(String.format(influxPubHost, devhost));
            slmPubFactory.setUri(String.format(slmPubHost, slmhost));
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException ex) {
            Logger.getLogger(StashHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {                    
            // Publisher exchange for Central
            pubconnection = centralPubFactory.newConnection();
            pubchannel = pubconnection.createChannel();
            pubchannel.exchangeDeclare("northbounddata", "topic");

            // Publisher exchange for Influx
            ipubconnection = influxPubFactory.newConnection();
            ipubchannel = ipubconnection.createChannel();
            ipubchannel.exchangeDeclare("northbounddata", "topic");

            // Publisher exchange for SLM
            slmpubconnection = slmPubFactory.newConnection();
            slmpubchannel = slmpubconnection.createChannel();
            slmpubchannel.exchangeDeclare("northbounddata", "topic");

            // Publisher exchange for TSM
            tsmpubconnection = centralPubFactory.newConnection();
            tsmpubchannel = tsmpubconnection.createChannel();
            tsmpubchannel.exchangeDeclare("tsm", "topic");

        } catch (IOException | TimeoutException ex) {
            Logger.getLogger(StashHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return true;
    }

    void onFile(File file) {
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(StashHandler.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null) {
                onMessage(line);
            }
            fileReader.close();
        } catch (IOException ex) {
            Logger.getLogger(StashHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    void onMessage(String message) {
        try {
            // Messages are of the format "X|chain.store.route|{some json message}"
            // Where X is the key to tell us what channel.
            String exchId=message.substring(0, 1);
            int nextSep=message.indexOf('|', 2);
            String key = message.substring(2, nextSep);
            String jsonstring = message.substring(nextSep+1);
            Channel channel;
            String exchangeName;
            switch (exchId) {
                case "T":
                    channel=tsmpubchannel;
                    exchangeName="tsm";
                    break;
                case "D":
                    channel=ipubchannel;
                    exchangeName="northbounddata";
                    break;
                case "S":
                    channel=slmpubchannel;
                    exchangeName="northbounddata";
                    break;
                case "R":
                    channel=pubchannel;
                    exchangeName="northbounddata";
                    break;
                default:
                    System.out.println("Invalid exchange ID received: " + exchId);
                    return;
            }
            try {
                System.out.println("Publishing Message to: " + exchangeName + " Message: " + jsonstring);
                channel.basicPublish(exchangeName, key, null, jsonstring.getBytes());
            } catch (java.lang.NullPointerException n) {
                System.out.println("Who is null? "+ exchangeName + " " + key + " " + jsonstring);            
            }
        } catch (IOException | java.lang.NullPointerException ex) {
            Logger.getLogger(StashHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void close() throws TimeoutException, IOException {
        pubconnection.close();
        pubchannel.close();

        ipubconnection.close();
        ipubchannel.close();

        tsmpubconnection.close();
        tsmpubchannel.close();

        slmpubconnection.close();
        slmpubchannel.close();
    }    

    @SuppressWarnings("unchecked")
    static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>)event;
    }
 
    /**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE);
        if (trace) {
            Path prev = keys.get(key);
            if (prev == null) {
                System.out.format("register: %s\n", dir);
            } else {
                if (!dir.equals(prev)) {
                    System.out.format("update: %s -> %s\n", prev, dir);
                }
            }
        }
        keys.put(key, dir);
    }
 
    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                throws IOException
            {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
 
    /**
     * Creates a WatchService and registers the given directory
     */
    StashHandler(Path dir, boolean recursive) throws IOException {
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<>();
        this.recursive = recursive;
 
        if (recursive) {
            System.out.format("Scanning %s ...\n", dir);
            registerAll(dir);
            System.out.println("Done.");
        } else {
            register(dir);
        }
 
        // enable trace after initial registration
        this.trace = true;
    }
 
    /**
     * Process all events for keys queued to the watcher
     */
    void processEvents() {
        for (;;) {
 
            // wait for key to be signalled
            WatchKey key;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return;
            }
 
            Path dir = keys.get(key);
            if (dir == null) {
                System.err.println("WatchKey not recognized!!");
                continue;
            }
 
            key.pollEvents().forEach(new java.util.function.Consumer<WatchEvent<?>>() {
                @Override
                public void accept(WatchEvent<?> event) {
                    WatchEvent.Kind kind = event.kind();
                    // TBD - provide example of how OVERFLOW event is handled
                    if (!(kind == OVERFLOW)) {
                        // Context for directory entry event is the file name of entry
                        WatchEvent<Path> ev = cast(event);
                        Path name = ev.context();
                        Path child = dir.resolve(name);
                        
                        // print out event
                        System.out.format("%s: %s\n", event.kind().name(), child);
                        
                        // if directory is created, and watching recursively, then
                        // register it and its sub-directories
                        if (recursive && (kind == ENTRY_CREATE) && Files.isDirectory(child, NOFOLLOW_LINKS)) {
                            try {
                                registerAll(child);
                            } catch (IOException ex) {
                                // ignore to keep sample readbale
                            }
                        }
                        else if ((kind == ENTRY_CREATE) && Files.isRegularFile(child)) {
                            try {
                                // We have to do this to make sure the file is fully written.
                                File lockFile = new File(child.toString());
                                FileChannel channel = new RandomAccessFile( lockFile, "rw" ).getChannel( );
                                channel.lock();
                                onFile(lockFile);
                                
                            } catch (FileNotFoundException ex) {
                                Logger.getLogger(StashHandler.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (IOException ex) {
                                Logger.getLogger(StashHandler.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }
                }
            });
 
            // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);
 
                // all directories are inaccessible
                if (keys.isEmpty()) {
                    break;
                }
            }
        }
    }
}
