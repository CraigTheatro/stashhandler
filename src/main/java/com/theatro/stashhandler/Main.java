/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.theatro.stashhandler;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.logging.LogRecord;

/**
 *
 * @author cmiller
 */
public class Main {
    
    public static final Logger LOGGER = Logger.getLogger("THEATRO");

    static void usage() {
        System.err.println("usage: java StashHandler [-r] dir");
        System.exit(-1);
    }

    public static void main(String[] args) throws InterruptedException, IOException, Exception {
        FileHandler fileHandler = new FileHandler("/opt/theatro/logs/StashHandler.log", true);
        LOGGER.addHandler(fileHandler);
        fileHandler.setFormatter(new SimpleFormatter(){
          private static final String format = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

          @Override
          public synchronized String format(LogRecord lr) {
              return String.format(format,
                      new Date(lr.getMillis()),
                      lr.getLevel().getLocalizedName(),
                      lr.getMessage()
              );
          }
        });
        
        fileHandler.setLevel(Level.ALL);
        LOGGER.setLevel(Level.ALL);
        LOGGER.log(Level.INFO, String.format("Stash Handler Started!"));
                
        // parse arguments
        if (args.length == 0 || args.length > 2)
            usage();
        boolean recursive = false;
        int dirArg = 0;
        if (args[0].equals("-r")) {
            if (args.length < 2)
                usage();
            recursive = true;
            dirArg++;
        }
 
        // register directory and process its events
        Path dir = Paths.get(args[dirArg]);
        new StashHandler(dir, recursive).processEvents();
    }    
}
